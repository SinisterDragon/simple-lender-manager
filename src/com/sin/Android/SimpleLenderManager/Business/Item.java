package com.sin.Android.SimpleLenderManager.Business;

import com.sin.Android.SimpleLenderManager.Database.LenderDataSource;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mav5228 on 12/5/13.
 */
public class Item implements Serializable {
    private String name;
    private String lendee;
    private String dateCheckOut;
    private String dateCheckIn;
    private long ID;

    public long getId() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLendee() {
        return lendee;
    }

    public void setLendee(String lendee) {
        this.lendee = lendee;
    }

    public String getDateCheckIn() {
        return dateCheckIn;
    }

    public void setDateCheckIn(String dateCheckIn) {
        this.dateCheckIn = dateCheckIn;
    }

    public String getDateCheckOut() {
        return dateCheckOut;
    }

    public String getDisplayDateCheckOut(){
        SimpleDateFormat dateParse = new SimpleDateFormat("MMddyyhhss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
        String date;

        try {
            Date dateparse = new Date();
            dateparse = dateParse.parse(dateCheckOut);
            date = dateFormat.format(dateparse);

        } catch (ParseException e) {
            date = dateCheckOut;
        }

        return date;
    }

    public void setDateCheckOut(String dateCheckOut) {
        this.dateCheckOut = dateCheckOut;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "Item - " + name + "\nLendee - " + lendee + " \nDate Out: " + getDisplayDateCheckOut() + "\n";
    }

}
