package com.sin.Android.SimpleLenderManager.Business;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;

/**
 * Created by mav5228 on 6/11/14.
 */
public class Utility {

    private final static String DIRECTORY = "SimpleLenderManager";

    /**
     * Get the album directory for the application
     * @return the directory to save the pictures
     */
    public static File getAlbumDirectory(){
        return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), DIRECTORY);
    }

    /**
     * Given a file, it will find the file in the media store to remove the picture from the device
     * and the gallery
     * @param context from which this method is called from
     * @param deletePicture picture which to be removed
     */
    public static void galleryDeletePic(Context context, File deletePicture) {
        // Set up the projection (we only need the ID)
        String[] projection = {MediaStore.Images.Media._ID};

        // Match on the file path
        String selection = MediaStore.Images.Media.DATA + " = ?";
        String[] selectionArgs = new String[]{deletePicture.getAbsolutePath()};

        // Query for the ID of the media matching the file path
        Uri queryUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        ContentResolver contentResolver = context.getContentResolver();
        Cursor c = contentResolver.query(queryUri, projection, selection, selectionArgs, null);
        if (c.moveToFirst()) {
            // We found the ID. Deleting the item via the content provider will also remove the file
            long id = c.getLong(c.getColumnIndexOrThrow(MediaStore.Images.Media._ID));
            Uri deleteUri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, id);
            contentResolver.delete(deleteUri, null, null);
        } else {
            // File not found in media store DB
        }
        c.close();

    }

    public static String buildFileNameFromItem(Item item){
        return item.getName() + item.getLendee() + "_" + item.getDateCheckOut() + ".jpg";
    }

}
