package com.sin.Android.SimpleLenderManager.Screens
        ;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.sin.Android.SimpleLenderManager.Business.Item;
import com.sin.Android.SimpleLenderManager.Business.ItemImage;
import com.sin.Android.SimpleLenderManager.Database.LenderDataSource;
import com.sin.Android.SimpleLenderManager.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mav5228 on 6/4/14.
 */
public class LenderAddItem extends Activity {
    private final static int TAKE_PICTURE = 1;
    private LenderDataSource datasource;
    private EditText name, itemText;
    private ImageView imageView;
    private String date;
    private ItemImage itemImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyyhhmmss");
        date = dateFormat.format(new Date());
        setContentView(R.layout.add_dialog);
        datasource = new LenderDataSource(this);
        datasource.open();
        name = (EditText) findViewById(R.id.editText);
        itemText = (EditText) findViewById(R.id.editText2);
        imageView = (ImageView) findViewById(R.id.imageView);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item_add, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_add:

                Intent itemResult = new Intent();
                itemResult.putExtra("item", datasource.createItem(setupItem()));
                setResult(Activity.RESULT_OK, itemResult);

                datasource.close();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Item setupItem(){
        Item sendItem = new Item();
        sendItem.setLendee(itemText.getText().toString());
        sendItem.setName(name.getText().toString());
        sendItem.setDateCheckOut(date);
        return sendItem;
    }


    public void onClick(View view) {
        switch (view.getId()) {


            case R.id.takePicture:
                if (name.getText().toString().equals("") || itemText.getText().toString().equals("")) {
                    Toast.makeText(this, "Please enter an Item name and Lendee Please", Toast.LENGTH_SHORT).show();
                } else {
                    takePicture();
                }

                break;
        }

    }



    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            itemImage = new ItemImage(name.getText().toString(), itemText.getText().toString(), date);
            File image = itemImage.setUpPhotoFile();
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, TAKE_PICTURE);
        }

    }

    private void setCameraPhoto() {

        if (itemImage.getPath() != null) {
            imageView.setImageBitmap(itemImage.getBitmap(this));
            imageView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == RESULT_OK) {
                    setCameraPhoto();
                } else if (resultCode == RESULT_CANCELED) {

                } else {
                    Toast.makeText(this, "Error getting Image",
                            Toast.LENGTH_SHORT).show();
                }
        }

    }


}
