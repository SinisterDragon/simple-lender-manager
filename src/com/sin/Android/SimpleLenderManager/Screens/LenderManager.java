package com.sin.Android.SimpleLenderManager.Screens;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.sin.Android.SimpleLenderManager.Business.ChangeLog;
import com.sin.Android.SimpleLenderManager.Business.Item;
import com.sin.Android.SimpleLenderManager.Business.Utility;
import com.sin.Android.SimpleLenderManager.Database.LenderDataSource;
import com.sin.Android.SimpleLenderManager.R;

import java.io.File;
import java.util.List;

public class LenderManager extends ListActivity {
    final Context context = this;
    private LenderDataSource dataSource;
    private ChangeLog cl = null;
    private Item lastClicked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        cl = new ChangeLog(this);
        if(cl.firstRunEver() || cl.firstRun()){
            cl.getFullLogDialog().show();
        }

        dataSource = new LenderDataSource(this);
        dataSource.open();
        List<Item> values = dataSource.getAllItems();

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
        ArrayAdapter<Item> adapter = new ArrayAdapter<Item>(this,
                R.layout.row_layout, values);
        setListAdapter(adapter);
        ListView lv = getListView();
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> av, View v, int pos, long id) {
                return onLongListItemClick(v, pos, id);
            }
        });
    }

    public void onListItemClick(ListView parent, View v, int position, long id) {
        lastClicked = (Item) getListAdapter().getItem(position);
        Intent k = new Intent(context, LenderItemDetails.class);
        k.putExtra("item", lastClicked);
        startActivityForResult(k, 2);
    }


    public boolean onLongListItemClick(View v, int position, long id) {

        final Item item = (Item) getListAdapter().getItem(position);

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Delete?");
        alert.setMessage("Are you sure you want to delete?");

        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ArrayAdapter<Item> adapter = (ArrayAdapter<Item>) getListAdapter();

                dataSource.deleteItem(item);
                adapter.remove(item);
                adapter.notifyDataSetChanged();
                deletePicture(item);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
        return true;
    }

    private void deletePicture(Item item){
        File file = new File(Utility.getAlbumDirectory(), Utility.buildFileNameFromItem(item));
        if(file.exists()){
            Utility.galleryDeletePic(this, file);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_add_menu:
                Intent k = new Intent(context, LenderAddItem.class);
                startActivityForResult(k, 1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
        dataSource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        dataSource.close();
        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                ArrayAdapter<Item> adapter = (ArrayAdapter<Item>) getListAdapter();
                Item result = (Item) data.getExtras().get("item");
                adapter.add(result);
                adapter.notifyDataSetChanged();
            }
        }else if (requestCode == 2){
            if(resultCode == LenderItemDetails.RESULT_DELETE){
                ArrayAdapter<Item> adapter = (ArrayAdapter<Item>) getListAdapter();
                adapter.remove(lastClicked);

                adapter.notifyDataSetChanged();
            }

        }
    }

}
