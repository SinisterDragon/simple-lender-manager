package com.sin.Android.SimpleLenderManager.Screens;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.sin.Android.SimpleLenderManager.Business.Item;
import com.sin.Android.SimpleLenderManager.Business.Utility;
import com.sin.Android.SimpleLenderManager.Database.LenderDataSource;
import com.sin.Android.SimpleLenderManager.R;

import java.io.File;
import java.io.IOException;

/**
 * Created by mav5228 on 6/5/14.
 */
public class LenderItemDetails extends Activity {
    private final static String DIRECTORY = "/SimpleLenderManager/";
    private Item item;
    private TextView nameOfItem, lendee, dateOut;
    private ImageView image;
    private int height, width;
    private String filePath;
    Bitmap thumb = null;
    public static int RESULT_DELETE = 99;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.item_details);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        item = (Item) getIntent().getExtras().get("item");
        onAfterCreate();

    }

    private void onAfterCreate() {
        nameOfItem = (TextView) findViewById(R.id.nameofItem);
        lendee = (TextView) findViewById(R.id.lendee);
        dateOut = (TextView) findViewById(R.id.dateOut);
        image = (ImageView) findViewById(R.id.imageView);
        setUpLabels();
        takePicture();

    }

    private void setUpLabels() {
        nameOfItem.setText(item.getName());
        lendee.setText(item.getLendee());
        dateOut.setText(item.getDisplayDateCheckOut());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.item_details_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        // Handle presses on the action bar items
        switch (menuItem.getItemId()) {
            case R.id.action_delete:
                final LenderDataSource dataSource = new LenderDataSource(this);
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Delete?");
                alert.setMessage("Are you sure you want to delete?");

                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dataSource.open();
                        dataSource.deleteItem(item);

                        setResult(RESULT_DELETE);
                        File deletePicture = new File(filePath);

                        if (deletePicture.exists()) {
                            deletePicture(deletePicture);
                        }

                        dataSource.close();
                        finish();
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                    }
                });

                alert.show();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void deletePicture(File deletePicture){
        Utility.galleryDeletePic(this, deletePicture);
    }
 

    private void takePicture() {

        String photoFile = item.getName() + item.getLendee() + "_" + item.getDateCheckOut() + ".jpg";
        filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + DIRECTORY +
                photoFile;

        getScaledBitmap();

    }

    private void getScaledBitmap() {


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 4;

        thumb = BitmapFactory.decodeFile(filePath, options);
        if (thumb == null) {
            return;
        }
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;
        Matrix matrix = new Matrix();
        matrix.postRotate(rotationAngle);

        Bitmap rotated = Bitmap.createBitmap(thumb, 0, 0, thumb.getWidth(), thumb.getHeight(),
                matrix, true);
        thumb = rotated;
        height = thumb.getHeight();
        width = thumb.getWidth();


        image.setImageBitmap(thumb);
        image.setVisibility(View.VISIBLE);
    }
}
